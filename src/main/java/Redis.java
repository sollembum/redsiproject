import redis.clients.jedis.Jedis;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Redis {
    private static HashMap<String,String> aneks = new HashMap<String, String>();
    private static List<String> likes = new ArrayList();

    public static void main(String[] args) throws FileNotFoundException {
        Jedis jedis = new Jedis("localhost", 6379);

        String filePath = "src/main/resources/aneks.txt";
        Scanner scanner = new Scanner(new File(filePath));
        String str;
        String anek = "";
        while (scanner.hasNextLine()){
            str = scanner.nextLine();
            Matcher matcher = getPattern("[0-9][\\/][\\/]").matcher(str);
            if (matcher.find()){
                likes.add(str.substring(0,str.length()-2));
                String key = likes.get(likes.size()-1);//ключ есть кол-во лайков на анеке
                aneks.put(key, anek);
                anek = "";
            }
            else{
                anek = anek + "\n" + str;
            }
        }

        for (int i = 0; i < aneks.size(); i++) {
            jedis.set(likes.get(i), aneks.get(likes.get(i)));
        }
        if(w8Command()){
            System.out.println(getRandomAnek(jedis,likes));
        }
    }

    public static Pattern getPattern(String regexp) {
        Pattern pattern = Pattern.compile(regexp);
        return pattern;
    }

    public static String getRandomAnek(Jedis jedis,List<String> keys){
        String key = keys.get((int) (Math.random()*keys.size()));
        return jedis.get(key);
    }
    public static boolean w8Command(){
        System.out.println("Enter Y if you want anek");
        Scanner in = new Scanner(System.in);
        String command = in.nextLine();
        if (command.toLowerCase().equals("y")){
            in.close();
            return true;
        }
        return false;
    }
}
